import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FeedbackService } from '../../services/UX/feedback.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  test: Date = new Date();
  logintype = "login";
  nome: String;

  constructor(private router: Router,
    public feedback: FeedbackService) { }

  ngOnInit() {
  }

  login(form) {
    console.log('Usuário e senha: ', form.value);
    this.nome = form.value.nome;
    sessionStorage.setItem('nome', form.value.nome);
    this.welcome();
    this.router.navigate(['home']);
  }

  changeLoginType(type: string) {
    this.logintype = type;
  }

  welcome() {
    let toaster = {
      message: '',
      duration: 2000,
      position: 'top',
      color: 'danger',
    }
    this.nome == undefined ? toaster.message = 'Bem vindo!' : toaster.message = 'Bem vindo ' + this.nome; 

    this.feedback.createToaster(toaster);

  }

}
