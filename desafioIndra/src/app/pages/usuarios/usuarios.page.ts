import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FeedbackService } from 'src/app/services/UX/feedback.service';
import { UsersCrudService } from 'src/app/services/usersCrud/users-crud.service';


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.page.html',
  styleUrls: ['./usuarios.page.scss'],
})
export class UsuariosPage implements OnInit {
  users: any;

  constructor(private router: Router, public feedback: FeedbackService, public usersCrud: UsersCrudService) { }

  ngOnInit() {
    this.getUsers();;
  }

  goto(page){
    this.router.navigate([page]);
    page == 'login' ? this.goodBye() : console.log('Navegando para ', page);
  }

  goodBye(){
    let toaster = {
      message: 'Até a próxima!',
      duration: 2000,
      position: 'top',
      color: 'warning',
    }
    this.feedback.createToaster(toaster);

  }

  removeUser(nome){
    this.feedback.deleteSwal(nome);    

  }

  editUser(){

  }

  addUser(){
    
  }

  getUsers(){
    this.usersCrud.getAll().subscribe(data => {
      console.log(data);
       this.users = data;
    });
  }


}
