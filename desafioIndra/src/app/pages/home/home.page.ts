import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FeedbackService } from '../../services/UX/feedback.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(private router: Router,  public feedback: FeedbackService) { }

  ngOnInit() {
  }

  goto(page){
    this.router.navigate([page]);
    page == 'login' ? this.goodBye() : console.log('Navegando para ', page);
  }

  goodBye(){
    let toaster = {
      message: 'Até a próxima!',
      duration: 2000,
      position: 'top',
      color: 'warning',
    }
    this.feedback.createToaster(toaster);

  }

}
