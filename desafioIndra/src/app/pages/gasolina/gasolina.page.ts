import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FeedbackService } from 'src/app/services/UX/feedback.service';
import Chart from 'chart.js'

@Component({
  selector: 'app-gasolina',
  templateUrl: './gasolina.page.html',
  styleUrls: ['./gasolina.page.scss'],
})
export class GasolinaPage implements OnInit {

  
  
  constructor(private router: Router, public feedback: FeedbackService) { }

  ngOnInit() {
    this.generateChart();
  }

  goto(page){
    this.router.navigate([page]);
    page == 'login' ? this.goodBye() : console.log('Navegando para ', page);
  }

  goodBye(){
    let toaster = {
      message: 'Até a próxima!',
      duration: 2000,
      position: 'top',
      color: 'warning',
    }
    this.feedback.createToaster(toaster);

  }

  generateChart(){
    var body = document.getElementsByTagName("body")[0];
    body.classList.add("landing-page");
    var canvas: any = document.getElementById("chartBig");
    var ctx = canvas.getContext("2d");
    var gradientFill = ctx.createLinearGradient(0, 350, 0, 50);
    gradientFill.addColorStop(0, "rgba(228, 76, 196, 0.0)");
    gradientFill.addColorStop(1, "rgba(228, 76, 196, 0.14)");
    var chartBig = new Chart(ctx, {
      type: "line",
      responsive: true,
      data: {
        labels: [
          "JAN",
          "FEV",
          "MAR",
          "ABR",
          "MAI",
          "JUN",
          "JUL",
          "AGO",
          "SET",
          "OUT",
          "NOV",
          "DEZ"
        ],
        datasets: [
          {
            label: "Preço: R$",
            fill: true,
            backgroundColor: gradientFill,
            borderColor: "#fff700",
            borderWidth: 2,
            borderDash: [],
            borderDashOffset: 0.0,
            pointBackgroundColor: "#fff700",
            pointBorderColor: "rgba(255,255,255,0)",
            pointHoverBackgroundColor: "#be55ed",
            //pointHoverBorderColor:'rgba(35,46,55,1)',
            pointBorderWidth: 20,
            pointHoverRadius: 4,
            pointHoverBorderWidth: 15,
            pointRadius: 4,
            data: [3.68, 3.45, 3.47, 4.23, 4.5, 4.7, 3.99, 3.78, 3.21, 3.5, 3.43, 3.55]
          }
        ]
      },
      options: {
        maintainAspectRatio: true,
        legend: {
          display: true
        },

        tooltips: {
          backgroundColor: "#fff",
          titleFontColor: "#ff0000",
          bodyFontColor: "#666",
          bodySpacing: 0,
          xPadding: 4,
          mode: "nearest",
          intersect: 0,
          position: "nearest"
        },
        responsive: true,
        scales: {
          yAxes: [
            {
              barPercentage: 1.6,
              gridLines: {
                drawBorder: true,
                color: "rgba(0,0,0,0.0)",
                zeroLineColor: "#ffee00"
              },
              ticks: {
                display: true,
                suggestedMin: 0,
                suggestedMax: 5,
                padding: 0,
                fontColor: "#ffee00"
              }
            }
          ],

          xAxes: [
            {
              barPercentage: 1.6,
              gridLines: {
                drawBorder: false,
                color: "rgba(0,0,0,0)",
                zeroLineColor: "#ffee00"
              },
              ticks: {
                padding: 20,
                fontColor: "#ffee00"
              }
            }
          ]
        }
      }
    });
  }

}
