import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GasolinaPage } from './gasolina.page';

const routes: Routes = [
  {
    path: '',
    component: GasolinaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GasolinaPageRoutingModule {}
