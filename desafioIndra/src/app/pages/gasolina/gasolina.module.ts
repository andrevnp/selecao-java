import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GasolinaPageRoutingModule } from './gasolina-routing.module';

import { GasolinaPage } from './gasolina.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GasolinaPageRoutingModule
  ],
  declarations: [GasolinaPage]
})
export class GasolinaPageModule {}
