import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GasolinaPage } from './gasolina.page';

describe('GasolinaPage', () => {
  let component: GasolinaPage;
  let fixture: ComponentFixture<GasolinaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GasolinaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GasolinaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
