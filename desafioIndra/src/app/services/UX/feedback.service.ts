import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from '@ionic/angular';
import Swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  constructor( public toastController: ToastController,
    public loadingController: LoadingController,) { }
    
  //Toasts
  async createToaster(toaster) {
    const toast = await this.toastController.create({
      message: toaster.message,
      duration: toaster.duration,
      position: toaster.position,
      color: toaster.color,
    });
    toast.present();
  }

  //Loadings
async createLoading(loader: any){
  const loading = await this.loadingController.create({
    spinner: loader.spinner,
    message: loader.message,
    duration: loader.duration,    
     });
  await loading.present();

  const { role, data } = await loading.onDidDismiss();  
    }

    dismissLoading(){
      this.loadingController.dismiss();
    }

  //Swal Alerts

  deleteSwal(nome){
    Swal.fire({
      title: 'Você tem certeza?',
      text: "Você não poderá reverter isso!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, deletar!',
      cancelButtonText: 'Cancelar!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deletado!',
          'Operação realizada com sucesso',
          'success'
        )
      }
    })
  }





}
