import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class UsersCrudService {

  url = 'https://5eb717f4875f1a00167e1369.mockapi.io/combustivel/users';
  constructor(public http: HttpClient) { }


  // getAll() {
  //   this.http.get(this.url).map(res => res.json()).subscribe(data => {
  //     console.log(data);
  //   });
  // }
  getAll() {
    return this.http.get(this.url);
  }
}