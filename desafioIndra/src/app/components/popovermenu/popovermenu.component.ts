import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController, NavParams} from '@ionic/angular';
//import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';

@Component({
  selector: 'app-popovermenu',
  templateUrl: './popovermenu.component.html',
  styleUrls: ['./popovermenu.component.scss'],
})
export class PopovermenuComponent implements OnInit {

  constructor(private router: Router,
    private navParams: NavParams,
    private popoverController: PopoverController) { }

  ngOnInit() {}

  goto(page: String){
    this.router.navigate([page]);
    this.popoverController.dismiss();
  }


}
